# Vente en ligne Gecko

Projet de vente en ligne des matériels


## Prérequis

- [ ] Symfony_2
- [ ] Yarn
- [ ] Composer

### Dp

```
Docker_images nginx personalized
php-fpm
```

## Authors

* **ANDRIANANJAONINA Niriniaina** - *initialisation du projet* - [links](https://gitlab.com/Nir54/venteenlignegecko)