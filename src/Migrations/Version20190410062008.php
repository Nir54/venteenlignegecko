<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190410062008 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE caracteristiques_automoto ADD type_moteur VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE caracteristiques_automoto ADD embrayage VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE caracteristiques_automoto ADD nbreporte INT DEFAULT NULL');
        $this->addSql('ALTER TABLE caracteristiques_automoto ADD frein_avant VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE caracteristiques_automoto ADD frein_arriere VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE caracteristiques_automoto DROP type_moteur');
        $this->addSql('ALTER TABLE caracteristiques_automoto DROP embrayage');
        $this->addSql('ALTER TABLE caracteristiques_automoto DROP nbreporte');
        $this->addSql('ALTER TABLE caracteristiques_automoto DROP frein_avant');
        $this->addSql('ALTER TABLE caracteristiques_automoto DROP frein_arriere');
    }
}
