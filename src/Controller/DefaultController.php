<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Entity\Categorie;
use App\Entity\SousCategorie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{

     /**
     * @Route("/", name="defaults")
     */
    public function defaults(Request $request)
    {
        return $this->RedirectToRoute('default');
    }

    /**
     * @Route("/{_locale}", name="default", requirements={"_locale" = "%app.locales%"})
     */
    public function default(Request $request)
    {
        $locale = $request->getLocale();
        $cats = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $scats = $this->getDoctrine()->getRepository(SousCategorie::class)->findAll();
        $nbCat =  $this->getDoctrine()->getRepository(Categorie::class)->getCount_Categorie();

        $divDinamique = 100/$nbCat;

        return $this->render('default/default.html.twig', [
            'controller_name' => 'Gecko - Accueil',
            'categories'=>$cats,
            'sous_categories'=>$scats,
            'nbCat'=>$divDinamique
        ]);
    }

    /**
     * @Route("/autocomplete", name="autocomplete")
     */
    public function autocomplete()
    {
        $cats = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $scats = $this->getDoctrine()->getRepository(SousCategorie::class)->findAll();
        $prods = $this->getDoctrine()->getRepository(Produits::class)->findAll();

        $tab_autocomplete=[];

        foreach($cats as $cat){
            array_push( $tab_autocomplete,$cat->getCatLibelle());
        }

        foreach($scats as $scat){
            array_push( $tab_autocomplete,$scat->getScatLibelle());
        }

        foreach($prods as $prod){
            array_push( $tab_autocomplete,$prod->getProdName());
        }

        return new JsonResponse($tab_autocomplete);
    }
}
