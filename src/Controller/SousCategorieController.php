<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Entity\Categorie;
// use JMS\Serializer\SerializerBuilder;
use App\Entity\SousCategorie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SousCategorieController extends AbstractController
{
    /**
     * @Route("/{_locale}/souscategorie/{sousCat}", name="sous_categorie") , requirements={"_locale" = "%app.locales%"})
     */
    public function index(Request $request , SerializerInterface $serializer , $sousCat)
    {
        $cats = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $scats = $this->getDoctrine()->getRepository(SousCategorie::class)->findAll();
        $nbCat =  $this->getDoctrine()->getRepository(Categorie::class)->getCount_Categorie();
        $divDinamique = 100/$nbCat;
        
        // if ($request->getMethod() == 'POST') {
            // $id_sous_cat = $request->request->get('idSousCat');
            // $checkSousCat = $this->getDoctrine()->getRepository(SousCategorie::class)->findOneBy(['id' => $id_sous_cat]);
            $checkProduit = $this->getDoctrine()->getRepository(Produits::class)->findBy(['id_pro_scat' => $sousCat]);
        // }

        return $this->render('default/sous_categorie.html.twig', [
            'controller_name' => 'Gecko - Sous categorie - Prodiut',
            'categories'=>$cats,
            'sous_categories'=>$scats,
            'nbCat'=>$divDinamique,
            'produit'=>$checkProduit            
        ]);

        // $tab_produits=[];

        // foreach($checkProduit as $prod){
        //     array_push($tab_produits,$prod->getId().'#'.$prod->getProdName().'#'.$prod->getProdPrix());
        // }
        // return new JsonResponse($tab_produits);
    }
}
