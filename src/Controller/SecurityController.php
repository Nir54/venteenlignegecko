<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Utilisateurs;
use App\Entity\SousCategorie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_inscription")
     */
    public function inscription(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $utilisateur = new Utilisateurs();
            $data  = $request->request->get('dataForm');

            $checkMail = $this->getDoctrine()->getRepository(Utilisateurs::class)->findOneBy(['email' => $data['email_util']]);
            
            if ($checkMail==null){
                $utilisateur->setEmail($data['email_util']);
                $utilisateur->setUsername($data['nom_util']);
                $utilisateur->setPassword($data['mdp_util']);
                $em = $this -> getDoctrine()->getManager();
                $em -> persist($utilisateur);
                $em -> flush();

                $return = array('status' => 'ok');
            } else {
                $return = array('status' => 'ko');
            }
        }

        return new JsonResponse($return);
        
    }

    /**
     * @Route("/{_locale}/login", name="security_login")
     */
    public function login(Request $request)
    {
        $cats = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $scats = $this->getDoctrine()->getRepository(SousCategorie::class)->findAll();
        $nbCat =  $this->getDoctrine()->getRepository(Categorie::class)->getCount_Categorie();

        $divDinamique = 100/$nbCat;

        return $this->render('default/authentification.html.twig', [
            'etat' => 'login',
            'controller_name' => 'Gecko - Authentification',
            'categories'=>$cats,
            'sous_categories'=>$scats,
            'nbCat'=>$divDinamique
        ]);
    }

    /**
     * @Route("/registration", name="security_registration")
     */
    public function registration(Request $request)
    {
        return $this->render('default/authentification.html.twig', [
            'etat' => 'registration',
        ]);
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout(){}
    

}
