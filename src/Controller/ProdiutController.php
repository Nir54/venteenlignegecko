<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Entity\Categorie;
use App\Entity\SousCategorie;
use App\Entity\CaracteristiquesImmo;
use App\Entity\CaracteristiquesInfo;
use App\Entity\CaracteristiquesAutomoto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProdiutController extends AbstractController
{
    /**
     * @Route("/{_locale}/caracteristique/{idProduit}", name="caracteristique produit", requirements={"_locale" = "%app.locales%"})
     */
    public function caracteristique(Request $request , $idProduit)
    {
        $cats = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $scats = $this->getDoctrine()->getRepository(SousCategorie::class)->findAll();
        $nbCat =  $this->getDoctrine()->getRepository(Categorie::class)->getCount_Categorie();
        $checkproduit = $this->getDoctrine()->getRepository(Produits::class)->find($idProduit);
        $divDinamique = 100/$nbCat;

        $checksouscat= $this->getDoctrine()->getRepository(SousCategorie::class)->find($checkproduit->getIdProScat()->getId());
        $checkcat= $this->getDoctrine()->getRepository(Categorie::class)->find($checksouscat->getIdCatScat());

        $caract = $checkcat->getId();
        $tableCaracteristique_check;
        

        $em = $this->getDoctrine()->getManager();
        $champ=$em->getClassMetaData(CaracteristiquesAutomoto::class)->getColumnNames();
        // example1: creating a QueryBuilder instance
        //$qb = $em->createQueryBuilder();


        // TESTE ID CARACTERISTIQUE

        switch ($caract) {
            case 1:
                $tableCaracteristique_check = CaracteristiquesInfo::class;
                break;
            case 2:
                $tableCaracteristique_check = CaracteristiquesImmo::class;
                break;
            case 3:
                $tableCaracteristique_check = CaracteristiquesAutomoto::class;
                break;
        } 
        $checkCaracteristiqueByprod = $this->getDoctrine()->getRepository($tableCaracteristique_check)->findBy(['id_produit' => $idProduit]);
        //dump($checkCaracteristiqueByprod); 

        // dump($checkproduit);die;

        return $this->render('default/produit.html.twig', [
            'controller_name' => 'Gecko - Prodiut',
            'categories'=>$cats,
            'sous_categories'=>$scats,
            'nbCat'=>$divDinamique,
            'caracteristique'=>$checkCaracteristiqueByprod,
            'typesCaract'=>$caract,
            'produit_nom'=>$checkproduit->getProdName(),
            'champs'=>$champ
        ]);


    }
}
