<?php

namespace App\Repository;

use App\Entity\CaracteristiquesImmo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CaracteristiquesImmo|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaracteristiquesImmo|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaracteristiquesImmo[]    findAll()
 * @method CaracteristiquesImmo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaracteristiquesImmoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CaracteristiquesImmo::class);
    }

    // /**
    //  * @return CaracteristiquesImmo[] Returns an array of CaracteristiquesImmo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CaracteristiquesImmo
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
