<?php

namespace App\Repository;

use App\Entity\CaracteristiquesAutomoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CaracteristiquesAutomoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaracteristiquesAutomoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaracteristiquesAutomoto[]    findAll()
 * @method CaracteristiquesAutomoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaracteristiquesAutomotoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CaracteristiquesAutomoto::class);
    }

    // /**
    //  * @return CaracteristiquesAutomoto[] Returns an array of CaracteristiquesAutomoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CaracteristiquesAutomoto
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
