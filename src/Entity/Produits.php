<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitsRepository")
 */
class Produits
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prod_name;

    /**
     * @ORM\Column(type="float")
     */
    private $prod_prix;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SousCategorie", inversedBy="produits")
     */
    private $id_pro_scat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProdName(): ?string
    {
        return $this->prod_name;
    }

    public function setProdName(string $prod_name): self
    {
        $this->prod_name = $prod_name;

        return $this;
    }

    public function getProdPrix(): ?float
    {
        return $this->prod_prix;
    }

    public function setProdPrix(float $prod_prix): self
    {
        $this->prod_prix = $prod_prix;

        return $this;
    }

    public function getIdProScat(): ?SousCategorie
    {
        return $this->id_pro_scat;
    }

    public function setIdProScat(?SousCategorie $id_pro_scat): self
    {
        $this->id_pro_scat = $id_pro_scat;

        return $this;
    }

}
