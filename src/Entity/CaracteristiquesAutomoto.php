<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CaracteristiquesAutomotoRepository")
 */
class CaracteristiquesAutomoto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Marque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Modele;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $capaciteReservoir;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_produit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Embrayage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Nbreporte;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $frein_avant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $frein_arriere;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarque(): ?string
    {
        return $this->Marque;
    }

    public function setMarque(?string $Marque): self
    {
        $this->Marque = $Marque;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->Modele;
    }

    public function setModele(?string $Modele): self
    {
        $this->Modele = $Modele;

        return $this;
    }

    public function getCapaciteReservoir(): ?float
    {
        return $this->capaciteReservoir;
    }

    public function setCapaciteReservoir(?float $capaciteReservoir): self
    {
        $this->capaciteReservoir = $capaciteReservoir;

        return $this;
    }

    public function getIdProduit(): ?int
    {
        return $this->id_produit;
    }

    public function setIdProduit(?int $id_produit): self
    {
        $this->id_produit = $id_produit;

        return $this;
    }

    public function getEmbrayage(): ?string
    {
        return $this->Embrayage;
    }

    public function setEmbrayage(?string $Embrayage): self
    {
        $this->Embrayage = $Embrayage;

        return $this;
    }

    public function getNbreporte(): ?int
    {
        return $this->Nbreporte;
    }

    public function setNbreporte(?int $Nbreporte): self
    {
        $this->Nbreporte = $Nbreporte;

        return $this;
    }

    public function getFreinAvant(): ?string
    {
        return $this->frein_avant;
    }

    public function setFreinAvant(?string $frein_avant): self
    {
        $this->frein_avant = $frein_avant;

        return $this;
    }

    public function getFreinArriere(): ?string
    {
        return $this->frein_arriere;
    }

    public function setFreinArriere(?string $frein_arriere): self
    {
        $this->frein_arriere = $frein_arriere;

        return $this;
    }

}
