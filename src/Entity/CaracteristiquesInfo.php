<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CaracteristiquesInfoRepository")
 */
class CaracteristiquesInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Marque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Processeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Graphique;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CarteMere;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_produit;

    public function __construct()
    {
        $this->id_prod_carac_info = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarque(): ?string
    {
        return $this->Marque;
    }

    public function setMarque(?string $Marque): self
    {
        $this->Marque = $Marque;

        return $this;
    }

    public function getProcesseur(): ?string
    {
        return $this->Processeur;
    }

    public function setProcesseur(?string $Processeur): self
    {
        $this->Processeur = $Processeur;

        return $this;
    }

    public function getGraphique(): ?string
    {
        return $this->Graphique;
    }

    public function setGraphique(?string $Graphique): self
    {
        $this->Graphique = $Graphique;

        return $this;
    }

    public function getCarteMere(): ?string
    {
        return $this->CarteMere;
    }

    public function setCarteMere(string $CarteMere): self
    {
        $this->CarteMere = $CarteMere;

        return $this;
    }

    public function getIdProduit(): ?int
    {
        return $this->id_produit;
    }

    public function setIdProduit(?int $id_produit): self
    {
        $this->id_produit = $id_produit;

        return $this;
    }

}
