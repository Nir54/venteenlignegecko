<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SousCategorieRepository")
 */
class SousCategorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $scat_libelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="sousCategories")
     */
    private $id_cat_scat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Produits", mappedBy="id_pro_scat")
     */
    private $produits;

    public function __construct()
    {
        $this->produits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScatLibelle(): ?string
    {
        return $this->scat_libelle;
    }

    public function setScatLibelle(string $scat_libelle): self
    {
        $this->scat_libelle = $scat_libelle;

        return $this;
    }

    public function getIdCatScat(): ?Categorie
    {
        return $this->id_cat_scat;
    }

    public function setIdCatScat(?Categorie $id_cat_scat): self
    {
        $this->id_cat_scat = $id_cat_scat;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Produits $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
            $produit->setIdProScat($this);
        }

        return $this;
    }

    public function removeProduit(Produits $produit): self
    {
        if ($this->produits->contains($produit)) {
            $this->produits->removeElement($produit);
            // set the owning side to null (unless already changed)
            if ($produit->getIdProScat() === $this) {
                $produit->setIdProScat(null);
            }
        }

        return $this;
    }
}
