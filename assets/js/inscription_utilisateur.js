$(document).ready(function(){

    $('#bnt_inscription_util').click(function(e){
        e.preventDefault();

        var nom = $('#inscriNom').val();
        var mail = $('#inscriMail').val();
        var mdp = $('#inscriMdp').val();

        var dataForm = {
            "email_util" : mail,
            "nom_util" : nom,
            "mdp_util" : mdp,
        };
        var urlRegistration = "/inscription"

        $.ajax({
            type: "POST",
            url: urlRegistration,
            data: {dataForm: dataForm},
            success: function (data) {
                console.log(data);
                if( data.status == 'ok' ){
                    $('#modalInscription').modal("hide");
                } else {
                    alert('error');
                }
            },
            error: function () {
            }
        });


    });


    $('.user').click(function(){
        $('#modalLogin').modal({backdrop: 'static', keyboard: false})
    });

    $('#btnAnnulerLogin').click(function(){
        $('#modalLogin').modal("hide");
    });

    $('#link_inscription').click(function(){
        // $('#modalLogin').modal("hide");
        $('#modalInscription').modal({backdrop: 'static', keyboard: false})
    });

    $('#btnInscriptionAnnul').click(function(){
        $('#modalInscription').modal("hide");
        // $('#modalLogin').modal({backdrop: 'static', keyboard: false})
    });

    $('#link_mdp_oublie').click(function(){
        // $('#modalLogin').modal("hide");
        $('#modalRecupMdp').modal({backdrop: 'static', keyboard: false})
    });
    
    $('#btn_modal_annul_recup_mdp').click(function(){
        $('#modalRecupMdp').modal("hide");
        // $('#modalLogin').modal({backdrop: 'static', keyboard: false})
    });
    

    


});