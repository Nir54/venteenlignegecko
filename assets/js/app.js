/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');
require('bootstrap/dist/css/bootstrap.min.css');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('../css/style_entete_pied.scss');
require('../css/style_inscription_login.scss');
require('../css/style_content_block1.scss');
require('../css/style_global.scss');
require('../css/autocomplete.min.css');




// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('../js/tete_page.js');
require('bootstrap/dist/js/bootstrap.min.js');
require('../js/inscription_utilisateur.js');
require('../js/inputValidator.js');
require('../js/autocomplete.min.js');

// AUTOCOMPLETION

var data_autocomplete=[];
$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: "/autocomplete",
        // data: {dataForm: dataForm},
        success: function (data) {
            console.log(data);
            $.each(data, function( key, value ) {
                // console.log( key + ": " + value );
                data_autocomplete.push(value);
            });
        },
        error: function () {
        }
    });

    $(".inputTeteRecherche").autocomplete({
      source: data_autocomplete
    });
});


// menu vertical
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();